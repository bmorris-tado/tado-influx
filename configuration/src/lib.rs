use config::{Config, Environment, File};
use serde_derive::Deserialize;
use std::collections::BTreeSet;
use std::env;
use url::Url;

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct TadoApiProps {
    pub baseurl: Url,
    pub oauth: Option<TadoOauthProps>,
    pub credentials: Option<TadoCredentials>,
}

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct TadoOauthProps {
    pub tokenurl: Url,
    pub clientid: String,
    pub clientsecret: String,
    pub scopes: BTreeSet<String>,
}

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct TadoCredentials {
    pub username: String,
    pub password: String,
}

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct Influx {
    pub url: Url,
    pub org: String,
    pub bucket: String,
    pub authtoken: String,
}

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct Metrics {
    pub host: String,
    pub path: String,
    pub port: u16,
}

#[derive(Debug, Clone, Deserialize)]
#[allow(unused)]
pub struct Settings {
    pub tadoapi: Option<TadoApiProps>,
    pub influx: Option<Influx>,
    pub metrics: Option<Metrics>,
}

impl Settings {
    pub fn new() -> anyhow::Result<Self> {
        let run_mode = env::var("RUN_MODE").unwrap_or_else(|_| "development".into());

        let s = Config::builder()
            .add_source(File::with_name("application"))
            .add_source(File::with_name(&format!("application_{run_mode}")).required(false))
            .add_source(Environment::with_prefix("tado").separator("_"))
            .build()?;
        let settings = s.try_deserialize()?;
        Ok(settings)
    }
}
