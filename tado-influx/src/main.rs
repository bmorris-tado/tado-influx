#![forbid(unsafe_code)]

use arc_swap::ArcSwapOption;
use axum::{routing::get, Router};
use clokwerk::{AsyncScheduler, TimeUnits};
use configuration::Settings;
use futures::future::{try_join_all, Either};
use futures::{future::FutureExt, pin_mut};
use influx_client::InfluxClient;
use influxdb2::models::data_point::DataPointBuilder;
use influxdb2::models::DataPoint;
use opentelemetry::sdk::trace as sdktrace;
use opentelemetry::sdk::Resource;
use opentelemetry::trace::TraceError;
use opentelemetry_otlp::WithExportConfig;
use opentelemetry_semantic_conventions::resource::{PROCESS_COMMAND, PROCESS_PID, SERVICE_NAME};
use prometheus::{Encoder, TextEncoder};
use std::net::ToSocketAddrs;
use std::sync::Arc;
use std::time::Duration;
use tado_api::models::GenericZoneSetting::HeatingZoneSetting;
use tado_api::models::{
    MobileDevice, PercentageDataPoint, TemperatureDataPoint, User, UserHomesInner, Zone, ZoneState,
};
use tado_client::TadoClient;
use time::OffsetDateTime;
use tokio::sync::Mutex;
use tracing::{debug, error, info, instrument, trace, warn, Level};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

fn init_tracer() -> Result<sdktrace::Tracer, TraceError> {
    let context = Resource::new(vec![
        SERVICE_NAME.string("tado-influx"),
        PROCESS_PID.i64(std::process::id() as i64),
        PROCESS_COMMAND.string(
            std::env::current_exe()
                .map(|e| e.display().to_string())
                .unwrap_or_else(|_| "N/A".to_string()),
        ),
    ]);

    opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_exporter(opentelemetry_otlp::new_exporter().tonic().with_env())
        .with_trace_config(sdktrace::config().with_resource(Resource::default().merge(&context)))
        .install_batch(opentelemetry::runtime::Tokio)
}

fn init_tracing() {
    let tracer = init_tracer().expect("Failed to create OTEL tracer");

    tracing_subscriber::fmt()
        .json()
        .with_span_list(true)
        .with_max_level(Level::INFO)
        .finish()
        .with(tracing_opentelemetry::layer().with_tracer(tracer))
        .init();
}

fn tado_client(settings: &Settings) -> anyhow::Result<TadoClient> {
    TadoClient::create(settings)
}

fn influx_client(settings: &Settings) -> anyhow::Result<InfluxClient> {
    InfluxClient::from_settings(settings.clone())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    init_tracing();

    let settings = settings().expect("Failed to read settings");

    let t1 = start_metrics_server(&settings).fuse();
    let t2 = start_poll_tado(&settings).fuse();
    pin_mut!(t1, t2);

    match futures::future::select(t1, t2).await {
        Either::Left((left, _)) => left,
        Either::Right((right, _)) => right,
    }
}

async fn start_metrics_server(settings: &Settings) -> anyhow::Result<()> {
    let metrics = settings.metrics.as_ref().unwrap();
    let app = Router::new().route(
        metrics.path.as_str(),
        get(|| async {
            let mut buffer = vec![];
            let encoder = TextEncoder::new();
            let metric_families = prometheus::gather();
            encoder.encode(&metric_families, &mut buffer).unwrap();
            String::from_utf8(buffer).unwrap()
        }),
    );

    let host = format!("{}:{}", metrics.host, metrics.port);
    let addr = host
        .to_socket_addrs()
        .expect("Invalid metrics host:port")
        .next()
        .expect("No address specified");
    info!("listening on {}", addr);
    let srv = axum::Server::bind(&addr).serve(app.into_make_service());

    srv.await?;
    Ok(())
}

async fn start_poll_tado(settings: &Settings) -> anyhow::Result<()> {
    let tado_client = tado_client(settings).expect("Failed to create Tado client");
    let influx_client = influx_client(settings).expect("Failed to create influx client");
    let tado_influx = TadoInflux::from(tado_client, influx_client);

    let mut scheduler = AsyncScheduler::new();

    let arc = Arc::new(Mutex::new(tado_influx));

    let scheduler_arc = arc.clone();
    let update_user = move || {
        let scheduler_arc = scheduler_arc.clone();
        async move {
            let tado_influx = scheduler_arc.lock().await;
            tado_influx.update_user().await
        }
    };
    update_user().await;
    scheduler.every(5.minutes()).run(update_user);

    let poll_arc = arc.clone();
    scheduler.every(1.minutes()).run(move || {
        let poll_arc = poll_arc.clone();
        async move {
            let tado_influx = poll_arc.lock().await;
            tado_influx.poll_tado().await
        }
    });

    let fut = tokio::spawn(async move {
        loop {
            scheduler.run_pending().await;
            tokio::time::sleep(Duration::from_secs(1)).await;
        }
    });

    fut.await?;
    Ok(())
}

#[instrument]
fn settings() -> anyhow::Result<Settings> {
    let settings = Settings::new();
    info!("{:?}", settings);
    settings
}

#[derive(Debug)]
struct TadoInflux {
    tado_client: TadoClient,
    influx_client: InfluxClient,
    tado_user: ArcSwapOption<User>,
}

impl TadoInflux {
    fn from(tado_client: TadoClient, influx_client: InfluxClient) -> Self {
        Self {
            tado_client,
            influx_client,
            tado_user: ArcSwapOption::new(None),
        }
    }

    #[instrument(skip(self))]
    async fn update_user(&self) {
        let tado_client = &self.tado_client;
        let user = tado_client.show_user().await;
        info!("Got {:?}", user);
        if let Some(user) = user.unwrap() {
            self.tado_user.store(Some(Arc::new(user)))
        }
    }

    #[instrument(skip(self))]
    async fn poll_tado(&self) {
        let now = OffsetDateTime::now_utc().unix_timestamp();
        let arc = &self.tado_user;
        let user_option = arc.load();
        let user = user_option.as_ref().unwrap();
        info!("Polling for {:?}", user);
        let homes = user.homes.clone().unwrap_or_default();
        let futs: Vec<_> = homes
            .into_iter()
            .map(|home| self.poll_home(now, home))
            .collect();
        let results = try_join_all(futs).await;
        if results.is_err() {
            error!("Failed to poll homes {:?}", results)
        }
    }

    async fn poll_home(&self, unix_time: i64, home: UserHomesInner) -> anyhow::Result<()> {
        let data_points = self.get_metric_stream(unix_time, &home).await;
        let write = self.influx_client.write(data_points?);
        write.await?;
        Ok(())
    }

    async fn get_metric_stream(
        &self,
        unix_time: i64,
        home: &UserHomesInner,
    ) -> anyhow::Result<impl futures::Stream<Item = DataPoint>> {
        let mut points: Vec<DataPoint> = Vec::new();
        if let Some(home_state) = self.home_state(unix_time, home).await? {
            points.push(home_state);
        }
        points.append(&mut self.mobile_device_states(unix_time, home).await?);
        points.append(&mut self.zone_states(unix_time, home).await?);
        Ok(futures::stream::iter(points))
    }

    async fn home_state(
        &self,
        unix_time: i64,
        home: &UserHomesInner,
    ) -> anyhow::Result<Option<DataPoint>> {
        info!(
            "Polling for home {:?}",
            home.name.as_ref().unwrap_or(&"Unknown".to_string())
        );
        let tado_client = &self.tado_client;
        let home_state = tado_client.show_home_state(home).await?;
        info!("Got home state {:?}", &home_state);
        let point = home_state.and_then(|home_state| {
            DataPoint::builder("home-state")
                .timestamp(unix_time)
                .tag("home-id", home.id.as_ref().unwrap().to_string())
                .tag("name", home.name.as_ref().unwrap_or(&"Unknown".to_string()))
                .field("presence", home_state.presence.to_string())
                .build()
                .ok()
        });
        Ok(point)
    }

    async fn mobile_device_states(
        &self,
        unix_time: i64,
        home: &UserHomesInner,
    ) -> anyhow::Result<Vec<DataPoint>> {
        let tado_client = &self.tado_client;
        let mobile_devices = tado_client.list_mobile_devices(home).await?;
        info!("Got mobile devices {:?}", &mobile_devices);
        let points = mobile_devices
            .iter()
            .map(|device| {
                debug!("Processing mobile device {:?}", device.id);
                let point = TadoInflux::mobile_device_state(unix_time, home, device);
                trace!("For device {:?} point {:?}", device.id, &point);
                point.unwrap()
            })
            .collect();
        Ok(points)
    }

    fn mobile_device_state(
        unix_time: i64,
        home: &UserHomesInner,
        device: &MobileDevice,
    ) -> anyhow::Result<DataPoint> {
        let mut point = DataPoint::builder("device-state");
        point = point
            .timestamp(unix_time)
            .tag("home-id", home.id.as_ref().unwrap().to_string())
            .tag("device-id", device.id.as_ref().unwrap().to_string())
            .tag("name", device.name.to_string())
            .field(
                "geo-tracking-enabled",
                device.settings.geo_tracking_enabled.to_string(),
            );
        if let Some(location) = &device.location {
            point = point
                .field("at-home", location.at_home.to_string())
                .field("stale", location.stale.to_string());
        }
        let point = point.build()?;
        Ok(point)
    }

    async fn zone_states(
        &self,
        unix_time: i64,
        home: &UserHomesInner,
    ) -> anyhow::Result<Vec<DataPoint>> {
        let tado_client = &self.tado_client;
        let zones = tado_client.list_zones(home).await?;
        info!("Got zones {:?}", zones);

        let points: Vec<_> = zones
            .into_iter()
            .map(|zone| self.zone_points(unix_time, home, zone))
            .collect();
        let results = try_join_all(points).await?;
        Ok(results.into_iter().flatten().collect())
    }

    async fn zone_points(
        &self,
        unix_time: i64,
        home: &UserHomesInner,
        zone: Zone,
    ) -> anyhow::Result<Vec<DataPoint>> {
        let tado_client = &self.tado_client;
        let zone_state = tado_client.show_zone_state(home, &zone).await?;
        info!("Got zone state {:?}", zone_state);
        match zone_state {
            None => Ok(Vec::new()),
            Some(zone_state) => {
                self.zone_state_points(unix_time, home, zone, zone_state)
                    .await
            }
        }
    }

    async fn zone_state_points(
        &self,
        unix_time: i64,
        home: &UserHomesInner,
        zone: Zone,
        zone_state: ZoneState,
    ) -> anyhow::Result<Vec<DataPoint>> {
        let mut points: Vec<DataPoint> = Vec::new();
        let mut point = DataPoint::builder("zone-state")
            .timestamp(unix_time)
            .tag("home-id", home.id.as_ref().unwrap().to_string())
            .tag("zone-id", zone.id.to_string())
            .tag("name", zone.name.to_string())
            .tag("type", zone.zone_type.to_string())
            .field("zone-mode", zone_state.tado_mode.to_string())
            .field(
                "window-open",
                zone_state.open_window_detected.unwrap_or(false),
            )
            .field("link-state", zone_state.link.state.to_string());
        if let Some(link_reason) = zone_state.link.reason {
            if let Some(reason_code) = link_reason.code {
                point = point.field("link-reason-code", reason_code);
            }
            if let Some(reason_title) = link_reason.title {
                point = point.field("link-reason-title", reason_title);
            }
        }
        if let Some(open_window) = zone_state.open_window {
            if let Some(duration) = open_window.duration {
                point = point.field("open-window-total-duration", duration.as_secs() as i64);
            }
            if let Some(remaining_time) = open_window.remaining_time {
                point = point.field(
                    "open-window-remaining-duration",
                    remaining_time.as_secs() as i64,
                );
            }
        }
        if let Some(preparation) = zone_state.preparation {
            point = point.field("preparation-tado-mode", preparation.tado_mode.to_string());
            if let HeatingZoneSetting { power, temperature } = *preparation.setting {
                point = point.field("preparation-heating-state", power.to_string());
                if let Some(temperature) = temperature {
                    if let Some(celsius) = temperature.celsius {
                        point = point.field("preparation-target-temperature", f64::from(celsius));
                    }
                }
            } else {
                warn!(
                    "Unknown zone preparation setting type {:?}",
                    preparation.setting
                )
            };
        }
        if let HeatingZoneSetting { power, temperature } = *zone_state.setting {
            point = point.field("heating-state", power.to_string());
            if let Some(temperature) = temperature {
                if let Some(celsius) = temperature.celsius {
                    point = point.field("target-temperature", f64::from(celsius));
                }
            }
        } else {
            warn!("Unknown zone setting type {:?}", zone_state.setting)
        };

        let point = point.build();
        debug!("For zone {:?} point {:?}", zone.id, point);
        points.push(point?);

        if let Some(heating_power) = zone_state.activity_data_points.heating_power {
            points.push(TadoInflux::percentage_data_point(
                "zone-heating-power".to_string(),
                home,
                &zone,
                &heating_power,
            )?);
        }
        if let Some(humidity) = zone_state.sensor_data_points.humidity {
            points.push(TadoInflux::percentage_data_point(
                "zone-humidity".to_string(),
                home,
                &zone,
                &humidity,
            )?);
        }
        if let Some(inside_temperature) = zone_state.sensor_data_points.inside_temperature {
            points.push(TadoInflux::temperature_data_point(
                "zone-temperature".to_string(),
                home,
                &zone,
                &inside_temperature,
            )?);
        }
        Ok(points)
    }

    fn percentage_data_point(
        measurement: String,
        home: &UserHomesInner,
        zone: &Zone,
        data_point: &PercentageDataPoint,
    ) -> anyhow::Result<DataPoint> {
        let point = TadoInflux::data_point(measurement, data_point.timestamp.as_ref(), home, zone)?
            .field("value", f64::from(data_point.percentage))
            .build()?;
        Ok(point)
    }

    fn temperature_data_point(
        measurement: String,
        home: &UserHomesInner,
        zone: &Zone,
        data_point: &TemperatureDataPoint,
    ) -> anyhow::Result<DataPoint> {
        let point = TadoInflux::data_point(measurement, data_point.timestamp.as_ref(), home, zone)?
            .field("value", f64::from(data_point.celsius.unwrap_or(0.0)))
            .build()?;
        Ok(point)
    }

    fn data_point(
        measurement: String,
        timestamp: Option<&OffsetDateTime>,
        home: &UserHomesInner,
        zone: &Zone,
    ) -> anyhow::Result<DataPointBuilder> {
        let builder = DataPoint::builder(measurement)
            .timestamp(timestamp.unwrap().unix_timestamp())
            .tag("home-id", home.id.as_ref().unwrap().to_string())
            .tag("zone-id", zone.id.to_string());
        Ok(builder)
    }
}

#[cfg(test)]
mod tests {
    use crate::{influx_client, tado_client, TadoInflux};
    use configuration::{Influx, Settings, TadoApiProps, TadoCredentials, TadoOauthProps};
    use reqwest::Url;
    use simple_logger::SimpleLogger;
    use std::sync::Once;
    use stubr::Stubr;

    static INIT: Once = Once::new();

    fn setup() {
        INIT.call_once(|| {
            SimpleLogger::new().init().unwrap();
        });
    }

    #[tokio::test]
    #[stubr::mock]
    async fn test_poll_tado() {
        setup();
        let settings = test_settings(&stubr);

        let tado_client = tado_client(&settings).expect("Failed to create Tado client");
        let influx_client = influx_client(&settings).expect("Failed to create influx client");
        let tado_influx = TadoInflux::from(tado_client, influx_client);

        tado_influx.update_user().await;
        tado_influx.poll_tado().await;
    }

    fn test_settings(stubr: &Stubr) -> Settings {
        let tado_oauth = TadoOauthProps {
            tokenurl: Url::parse(&stubr.path("/token")).unwrap(),
            clientid: "test-client-id".to_string(),
            clientsecret: "test-client-secret".to_string(),
            scopes: vec!["test-scope-1".to_string(), "test-scope-2".to_string()]
                .into_iter()
                .collect(),
        };
        let tado_credentials = TadoCredentials {
            username: "test-user".to_string(),
            password: "test-user-password-123".to_string(),
        };

        let tado_api_props = TadoApiProps {
            baseurl: Url::parse(&stubr.path("/api/v2")).unwrap(),
            oauth: Some(tado_oauth),
            credentials: Some(tado_credentials),
        };
        let influx = Influx {
            url: Url::parse(&stubr.uri()).unwrap(),
            org: "org".to_string(),
            bucket: "bucket".to_string(),
            authtoken: "token".to_string(),
        };

        Settings {
            tadoapi: Some(tado_api_props),
            influx: Some(influx),
            metrics: None,
        }
    }
}
