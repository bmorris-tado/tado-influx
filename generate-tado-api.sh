#!/usr/bin/env bash

openapi-generator generate -g rust -i https://raw.githubusercontent.com/openhab/openhab-addons/main/bundles/org.openhab.binding.tado/src/main/api/tado-api.yaml -o tado-api --global-property packageName=tado_api --global-property packageVersion=2.0.0