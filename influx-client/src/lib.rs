use configuration::{Influx, Settings};
use futures_core::Stream;
use influxdb2::api::write::TimestampPrecision;
use influxdb2::models::WriteDataPoint;
use tracing::instrument;

#[derive(Clone, Debug)]
pub struct InfluxClient {
    settings: Influx,
    influx_client: influxdb2::Client,
}

impl InfluxClient {
    pub fn from_settings(settings: Settings) -> anyhow::Result<Self> {
        let influx = settings.influx.expect("Influx settings not configured");
        let influx_client = influxdb2::Client::new(
            influx.url.as_ref(),
            influx.org.to_string(),
            influx.authtoken.to_string(),
        );
        Ok(Self {
            settings: influx,
            influx_client,
        })
    }

    #[instrument(skip_all)]
    pub async fn write(
        &self,
        body: impl Stream<Item = impl WriteDataPoint> + Send + Sync + 'static,
    ) -> anyhow::Result<()> {
        self.influx_client
            .write_with_precision(&self.settings.bucket, body, TimestampPrecision::Seconds)
            .await?;
        Ok(())
    }
}
