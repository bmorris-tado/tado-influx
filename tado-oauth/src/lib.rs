use configuration::Settings;
use oauth2::basic::{BasicClient, BasicTokenResponse};
use oauth2::{
    AuthUrl, ClientId, ClientSecret, HttpRequest, HttpResponse, RefreshToken,
    ResourceOwnerPassword, ResourceOwnerUsername, Scope, TokenUrl,
};
use reqwest_middleware::ClientWithMiddleware;

#[derive(Clone)]
pub struct TadoOauthClient {
    settings: Settings,
    client: ClientWithMiddleware,
    delegate: BasicClient,
}

impl TadoOauthClient {
    pub fn from_settings(client: ClientWithMiddleware, settings: Settings) -> anyhow::Result<Self> {
        let tado_api = settings.tadoapi.as_ref();
        let oauth_props = tado_api
            .and_then(|api| api.oauth.as_ref())
            .expect("Could not find Tado OAuth props");
        let oauth_client = BasicClient::new(
            ClientId::new(oauth_props.clientid.to_string()),
            Some(ClientSecret::new(oauth_props.clientsecret.to_string())),
            AuthUrl::new("http://authorize".to_string())?,
            Some(TokenUrl::new(oauth_props.tokenurl.to_string())?),
        );
        Ok(Self {
            settings,
            client,
            delegate: oauth_client,
        })
    }

    pub async fn get_token(&self) -> anyhow::Result<BasicTokenResponse> {
        let tado_api = self.settings.tadoapi.as_ref();

        let oauth_props = tado_api
            .and_then(|api| api.oauth.as_ref())
            .expect("Could not find Tado OAuth props");
        let credentials = tado_api
            .and_then(|api| api.credentials.as_ref())
            .expect("Could not find Tado credentials");
        let token = self
            .delegate
            .exchange_password(
                &ResourceOwnerUsername::new(credentials.username.to_string()),
                &ResourceOwnerPassword::new(credentials.password.to_string()),
            )
            .add_scopes(
                oauth_props
                    .scopes
                    .iter()
                    .map(|scope| Scope::new(scope.to_owned())),
            )
            .request_async(|r| self.async_http_client(r))
            .await?;
        Ok(token)
    }

    pub async fn refresh_token(
        &self,
        refresh_token: &RefreshToken,
    ) -> anyhow::Result<BasicTokenResponse> {
        let token = self
            .delegate
            .exchange_refresh_token(refresh_token)
            .request_async(|r| self.async_http_client(r))
            .await?;
        Ok(token)
    }

    async fn async_http_client(
        &self,
        request: HttpRequest,
    ) -> Result<HttpResponse, reqwest_middleware::Error> {
        let mut request_builder = self
            .client
            .request(request.method, request.url.as_str())
            .body(request.body);
        for (name, value) in &request.headers {
            request_builder = request_builder.header(name.as_str(), value.as_bytes());
        }
        let request = request_builder.build()?;

        let response = self.client.execute(request).await?;

        let status_code = response.status();
        let headers = response.headers().to_owned();
        let chunks = response.bytes().await?;
        Ok(HttpResponse {
            status_code,
            headers,
            body: chunks.to_vec(),
        })
    }
}

#[cfg(test)]
mod tests {
    use crate::TadoOauthClient;
    use configuration::{Settings, TadoApiProps, TadoCredentials, TadoOauthProps};
    use oauth2::{RefreshToken, TokenResponse};
    use reqwest::Url;
    use reqwest_middleware::ClientBuilder;
    use simple_logger::SimpleLogger;
    use std::sync::Once;
    use stubr::Stubr;

    static INIT: Once = Once::new();

    fn setup() {
        INIT.call_once(|| {
            SimpleLogger::new().init().unwrap();
        });
    }

    #[tokio::test]
    #[stubr::mock]
    async fn password_grant_test() {
        setup();
        let settings = test_settings(&stubr);

        let tado_auth_client = TadoOauthClient::from_settings(
            ClientBuilder::new(reqwest::Client::new()).build(),
            settings.clone(),
        )
        .unwrap();

        let token = tado_auth_client
            .get_token()
            .await
            .expect("Failed to get token");
        assert_eq!(token.access_token().secret(), "access-token")
    }

    #[tokio::test]
    #[stubr::mock]
    async fn refresh_token_test() {
        setup();
        let settings = test_settings(&stubr);

        let tado_auth_client = TadoOauthClient::from_settings(
            ClientBuilder::new(reqwest::Client::new()).build(),
            settings.clone(),
        )
        .unwrap();

        let refresh_token = RefreshToken::new("a-refresh-token".to_string());
        let token = tado_auth_client
            .refresh_token(&refresh_token)
            .await
            .expect("Failed to get token");
        assert_eq!(token.access_token().secret(), "refreshed-access-token")
    }

    fn test_settings(stubr: &Stubr) -> Settings {
        let credentials = TadoCredentials {
            username: "test-user".to_string(),
            password: "test-user-password-123".to_string(),
        };
        let oauth = TadoOauthProps {
            tokenurl: Url::parse(&stubr.path("/token")).unwrap(),
            clientid: "test-client-id".to_string(),
            clientsecret: "test-client-secret".to_string(),
            scopes: vec!["test-scope-1".to_string(), "test-scope-2".to_string()]
                .into_iter()
                .collect(),
        };

        let tado_api_props = TadoApiProps {
            baseurl: Url::parse(&stubr.path("/api/v2")).unwrap(),
            oauth: Some(oauth),
            credentials: Some(credentials),
        };

        Settings {
            tadoapi: Some(tado_api_props),
            influx: None,
            metrics: None,
        }
    }
}
