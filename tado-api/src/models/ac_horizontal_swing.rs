/*
 * tado° API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v2
 *
 * Generated by: https://openapi-generator.tech
 */

/// AcHorizontalSwing : Horizontal swing.

/// Horizontal swing.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum AcHorizontalSwing {
    #[serde(rename = "OFF")]
    Off,
    #[serde(rename = "ON")]
    On,
    #[serde(rename = "LEFT")]
    Left,
    #[serde(rename = "MID_LEFT")]
    MidLeft,
    #[serde(rename = "MID")]
    Mid,
    #[serde(rename = "MID_RIGHT")]
    MidRight,
    #[serde(rename = "RIGHT")]
    Right,
    #[serde(rename = "AUTO")]
    Auto,
}

impl ToString for AcHorizontalSwing {
    fn to_string(&self) -> String {
        match self {
            Self::Off => String::from("OFF"),
            Self::On => String::from("ON"),
            Self::Left => String::from("LEFT"),
            Self::MidLeft => String::from("MID_LEFT"),
            Self::Mid => String::from("MID"),
            Self::MidRight => String::from("MID_RIGHT"),
            Self::Right => String::from("RIGHT"),
            Self::Auto => String::from("AUTO"),
        }
    }
}

impl Default for AcHorizontalSwing {
    fn default() -> AcHorizontalSwing {
        Self::Off
    }
}
