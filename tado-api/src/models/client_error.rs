/*
 * tado° API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v2
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct ClientError {
    /// Error identifier
    #[serde(rename = "code")]
    pub code: String,
    /// Detailed error message. Just for informational purposes. Might change at any time!
    #[serde(rename = "title")]
    pub title: String,
}

impl ClientError {
    pub fn new(code: String, title: String) -> ClientError {
        ClientError { code, title }
    }
}
