/*
 * tado° API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v2
 *
 * Generated by: https://openapi-generator.tech
 */

use serde_with::{serde_as, DurationSeconds};
use std::time::Duration;
use time::OffsetDateTime;

#[serde_as]
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum OverlayTerminationCondition {
    #[serde(rename = "MANUAL")]
    ManualTerminationCondition {
        /// [ISO8601 datetime](https://en.wikipedia.org/wiki/ISO_8601). E.g. `2015-09-28T15:03:20Z` with second precision. Only relevant when receiving an overlay, ignored when overlay is sent to the server. Indicates the expected time of termination for this overlay, if no app user moves. `null` means that the overlay never expires (by itself, unless manully removed).
        #[serde(
            rename = "projectedExpiry",
            skip_serializing_if = "Option::is_none",
            with = "time::serde::iso8601::option"
        )]
        projected_expiry: Option<OffsetDateTime>,
    },
    #[serde(rename = "TADO_MODE")]
    TadoModeTerminationCondition {
        /// [ISO8601 datetime](https://en.wikipedia.org/wiki/ISO_8601). E.g. `2015-09-28T15:03:20Z` with second precision. Only relevant when receiving an overlay, ignored when overlay is sent to the server. Indicates the expected time of termination for this overlay, if no app user moves. `null` means that the overlay never expires (by itself, unless manully removed).
        #[serde(
            rename = "projectedExpiry",
            skip_serializing_if = "Option::is_none",
            with = "time::serde::iso8601::option"
        )]
        projected_expiry: Option<OffsetDateTime>,
    },
    #[serde(rename = "TIMER")]
    TimerTerminationCondition {
        /// [ISO8601 datetime](https://en.wikipedia.org/wiki/ISO_8601). E.g. `2015-09-28T15:03:20Z` with second precision. Only relevant when receiving an overlay, ignored when overlay is sent to the server. Indicates the expected time of termination for this overlay, if no app user moves. `null` means that the overlay never expires (by itself, unless manully removed).
        #[serde(
            rename = "projectedExpiry",
            skip_serializing_if = "Option::is_none",
            with = "time::serde::iso8601::option"
        )]
        projected_expiry: Option<OffsetDateTime>,
        /// The number of seconds that the overlay should last/was configured to last.
        #[serde(rename = "durationInSeconds")]
        #[serde_as(as = "DurationSeconds<u64>")]
        duration: Duration,
        /// [ISO8601 datetime](https://en.wikipedia.org/wiki/ISO_8601). E.g.: `2015-09-28T15:03:20Z` with second precision. Only relevant when receiving an overlay, ignored when overlay is sent to the server.
        #[serde(
            rename = "expiry",
            skip_serializing_if = "Option::is_none",
            with = "time::serde::iso8601::option"
        )]
        expiry: Option<OffsetDateTime>,
        /// The number of seconds that are remaining of the timer overlay at the time that the response is assembled by the server.
        #[serde(
            rename = "remainingTimeInSeconds",
            skip_serializing_if = "Option::is_none"
        )]
        #[serde_as(as = "Option<DurationSeconds<u64>>")]
        remaining_time: Option<Duration>,
    },
}
