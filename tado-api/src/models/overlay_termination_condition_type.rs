/*
 * tado° API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v2
 *
 * Generated by: https://openapi-generator.tech
 */

/// OverlayTerminationConditionType : When the overlay terminates. `MANUAL`: only when manually removed, `TADO_MODE`: when the current tado mode or the block schedule setting changes, `TIMER`: at a fixed point in time.

/// When the overlay terminates. `MANUAL`: only when manually removed, `TADO_MODE`: when the current tado mode or the block schedule setting changes, `TIMER`: at a fixed point in time.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum OverlayTerminationConditionType {
    #[serde(rename = "MANUAL")]
    Manual,
    #[serde(rename = "TADO_MODE")]
    TadoMode,
    #[serde(rename = "TIMER")]
    Timer,
}

impl ToString for OverlayTerminationConditionType {
    fn to_string(&self) -> String {
        match self {
            Self::Manual => String::from("MANUAL"),
            Self::TadoMode => String::from("TADO_MODE"),
            Self::Timer => String::from("TIMER"),
        }
    }
}

impl Default for OverlayTerminationConditionType {
    fn default() -> OverlayTerminationConditionType {
        Self::Manual
    }
}
