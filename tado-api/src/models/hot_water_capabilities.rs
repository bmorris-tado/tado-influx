/*
 * tado° API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v2
 *
 * Generated by: https://openapi-generator.tech
 */

#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct HotWaterCapabilities {
    #[serde(rename = "type")]
    pub system_type: crate::models::TadoSystemType,
    /// True if hot water zone can set temperature, false otherwise.
    #[serde(rename = "canSetTemperature")]
    pub can_set_temperature: bool,
    #[serde(rename = "temperatures", skip_serializing_if = "Option::is_none")]
    pub temperatures: Option<Box<crate::models::TemperatureRange>>,
}

impl HotWaterCapabilities {
    pub fn new(
        system_type: crate::models::TadoSystemType,
        can_set_temperature: bool,
    ) -> HotWaterCapabilities {
        HotWaterCapabilities {
            system_type,
            can_set_temperature,
            temperatures: None,
        }
    }
}
