/*
 * tado° API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v2
 *
 * Generated by: https://openapi-generator.tech
 */

/// AcMode : Cooling system mode.

/// Cooling system mode.
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum AcMode {
    #[serde(rename = "COOL")]
    Cool,
    #[serde(rename = "HEAT")]
    Heat,
    #[serde(rename = "DRY")]
    Dry,
    #[serde(rename = "FAN")]
    Fan,
    #[serde(rename = "AUTO")]
    Auto,
}

impl ToString for AcMode {
    fn to_string(&self) -> String {
        match self {
            Self::Cool => String::from("COOL"),
            Self::Heat => String::from("HEAT"),
            Self::Dry => String::from("DRY"),
            Self::Fan => String::from("FAN"),
            Self::Auto => String::from("AUTO"),
        }
    }
}

impl Default for AcMode {
    fn default() -> AcMode {
        Self::Cool
    }
}
