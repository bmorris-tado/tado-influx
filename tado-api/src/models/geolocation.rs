/*
 * tado° API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v2
 *
 * Generated by: https://openapi-generator.tech
 */

/// Geolocation : Geolocation

#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct Geolocation {
    #[serde(rename = "latitude")]
    pub latitude: f64,
    #[serde(rename = "longitude")]
    pub longitude: f64,
}

impl Geolocation {
    /// Geolocation
    pub fn new(latitude: f64, longitude: f64) -> Geolocation {
        Geolocation {
            latitude,
            longitude,
        }
    }
}
