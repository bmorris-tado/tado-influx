/*
 * tado° API
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v2
 *
 * Generated by: https://openapi-generator.tech
 */

use serde_with::{serde_as, DurationSeconds};
use std::time::Duration;

#[serde_as]
#[derive(Clone, Debug, PartialEq, Default, Serialize, Deserialize)]
pub struct TimerTerminationConditionTemplateAllOf {
    /// The number of seconds that the overlay should last/was configured to last.
    #[serde(rename = "durationInSeconds")]
    #[serde_as(as = "DurationSeconds<u64>")]
    pub duration: Duration,
}

impl TimerTerminationConditionTemplateAllOf {
    pub fn new(duration: Duration) -> TimerTerminationConditionTemplateAllOf {
        TimerTerminationConditionTemplateAllOf { duration }
    }
}
