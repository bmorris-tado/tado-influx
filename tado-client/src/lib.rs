use anyhow::Result;
use arc_swap::ArcSwapOption;
use cached::{Cached, TimedCache};
use configuration::Settings;
use lazy_static::lazy_static;
use oauth2::basic::BasicTokenResponse;
use oauth2::http::HeaderValue;
use oauth2::TokenResponse;
use prometheus::{register_histogram_vec, HistogramVec};
use reqwest::header::{AUTHORIZATION, ETAG, IF_NONE_MATCH, USER_AGENT};
use reqwest::{Request, Response};
use reqwest_middleware::{ClientBuilder, Middleware, Next};
use reqwest_retry::policies::ExponentialBackoff;
use reqwest_retry::RetryTransientMiddleware;
use reqwest_tracing::TracingMiddleware;
use std::sync::{Arc, Mutex};
use std::time::{Duration, SystemTime};
use tado_api::apis::configuration::Configuration;
use tado_api::apis::home_api;
use tado_api::models::{HomeState, MobileDevice, User, UserHomesInner, Zone, ZoneState};
use tado_oauth::TadoOauthClient;
use task_local_extensions::Extensions;
use tracing::{debug, info, instrument};

#[derive(Clone, Debug)]
pub struct TadoClient {
    tado_configuration: Configuration,
}

impl TadoClient {
    pub fn create(settings: &Settings) -> anyhow::Result<Self> {
        let retry_policy = ExponentialBackoff::builder().build_with_max_retries(3);
        let client = ClientBuilder::new(reqwest::Client::new())
            .with(HttpMetricsMiddleware::new())
            .with(TracingMiddleware::default())
            .with(RetryTransientMiddleware::new_with_policy(retry_policy))
            .build();

        let tado_auth_client = TadoOauthClient::from_settings(client, settings.clone())?;

        let client = ClientBuilder::new(reqwest::Client::new())
            .with(HttpMetricsMiddleware::new())
            .with(TracingMiddleware::default())
            .with(RetryTransientMiddleware::new_with_policy(retry_policy))
            .with(TadoClientOauthMiddleware::from(tado_auth_client))
            .with(TadoEtagMiddleware::new())
            .build();

        let mut tado_configuration = Configuration::new(client);
        let tado_api_props = settings.tadoapi.as_ref();
        tado_configuration.base_path = tado_api_props.unwrap().baseurl.to_string();
        tado_configuration.user_agent = Some("tado-inflx".to_string());

        Ok(Self { tado_configuration })
    }

    #[instrument(skip(self))]
    pub async fn show_user(&self) -> Result<Option<User>> {
        let user = home_api::show_user(&self.tado_configuration).await?;
        Ok(user)
    }

    #[instrument(skip(self))]
    pub async fn show_home_state(&self, home: &UserHomesInner) -> Result<Option<HomeState>> {
        let id = i64::from(home.id.expect("Home has no id"));
        let home = home_api::home_state(&self.tado_configuration, id).await?;
        Ok(home)
    }

    #[instrument(skip(self))]
    pub async fn list_mobile_devices(&self, home: &UserHomesInner) -> Result<Vec<MobileDevice>> {
        let id = i64::from(home.id.expect("Home has no id"));
        let devices = home_api::list_mobile_devices(&self.tado_configuration, id).await?;
        Ok(devices)
    }

    #[instrument(skip(self))]
    pub async fn list_zones(&self, home: &UserHomesInner) -> Result<Vec<Zone>> {
        let id = i64::from(home.id.expect("Home has no id"));
        let devices = home_api::list_zones(&self.tado_configuration, id).await?;
        Ok(devices)
    }

    #[instrument(skip(self))]
    pub async fn show_zone_state(
        &self,
        home: &UserHomesInner,
        zone: &Zone,
    ) -> Result<Option<ZoneState>> {
        let home_id = i64::from(home.id.expect("Home has no id"));
        let zone_id = i64::from(zone.id);
        let zone = home_api::show_zone_state(&self.tado_configuration, home_id, zone_id).await?;
        Ok(zone)
    }
}

struct TokenWithTime {
    token: BasicTokenResponse,
    expiry: Option<SystemTime>,
}

impl TokenWithTime {
    fn from(token: BasicTokenResponse) -> Self {
        static ONE_MINUTE: Duration = Duration::from_secs(60);
        let expiry = token.expires_in().map(|expires_in| {
            let birthday = SystemTime::now();
            birthday + expires_in - ONE_MINUTE
        });
        Self { token, expiry }
    }

    fn is_expired(&self) -> bool {
        match self.expiry {
            None => false,
            Some(expiry) => SystemTime::now() > expiry,
        }
    }
}

struct TadoClientOauthMiddleware {
    tado_oauth_client: TadoOauthClient,
    token: ArcSwapOption<TokenWithTime>,
}

impl TadoClientOauthMiddleware {
    pub fn from(tado_oauth_client: TadoOauthClient) -> Self {
        Self {
            tado_oauth_client,
            token: ArcSwapOption::new(None),
        }
    }

    async fn get_or_load_token(&self) -> Result<()> {
        let arc = &self.token;
        let old = arc.load();
        if old.is_none() {
            info!("No token, requesting token");
            let token = self.tado_oauth_client.get_token().await;
            let arc = Arc::new(TokenWithTime::from(token?));
            self.token.store(Some(arc));
            return Ok(());
        }
        let token = old.as_ref().expect("No token");
        if token.is_expired() {
            info!("Expired token, refreshing token");
            let token = self
                .tado_oauth_client
                .refresh_token(token.token.refresh_token().expect("No refresh token"))
                .await;
            let arc = Arc::new(TokenWithTime::from(token?));
            self.token.store(Some(arc));
        }
        Ok(())
    }
}

#[async_trait::async_trait]
impl Middleware for TadoClientOauthMiddleware {
    async fn handle(
        &self,
        mut req: Request,
        extensions: &mut Extensions,
        next: Next<'_>,
    ) -> reqwest_middleware::Result<Response> {
        self.get_or_load_token().await?;
        let arc = self.token.load();
        let token = arc.as_ref().expect("No token");

        let mut auth_token = "Bearer ".to_owned();
        auth_token.push_str(token.token.access_token().secret());
        req.headers_mut().insert(
            AUTHORIZATION,
            HeaderValue::from_maybe_shared(auth_token).expect("No header value"),
        );
        next.run(req, extensions).await
    }
}

struct TadoEtagMiddleware {
    etags: Arc<Mutex<TimedCache<String, String>>>,
}

impl TadoEtagMiddleware {
    pub fn new() -> Self {
        Self {
            etags: Arc::new(Mutex::new(TimedCache::with_lifespan(30 * 60))),
        }
    }

    fn get_etag(&self, path: String) -> Option<String> {
        let lock = self.etags.lock();
        let mut etags = lock.unwrap();
        etags.cache_get(&path).cloned()
    }

    fn update_etag(
        &self,
        path: String,
        etag: Option<String>,
        response: &reqwest_middleware::Result<Response>,
    ) {
        if response.is_err() {
            return;
        }
        let response = response.as_ref().unwrap();
        let headers = response.headers();
        let header = headers.get(ETAG);
        debug!("Got ETag header {:?}", header);
        if let Some(header) = header {
            let new_etag = header.to_str().unwrap().to_string();
            match etag {
                None => {
                    debug!("No previous, setting Etag to {:?}", new_etag);
                    self.etags.lock().unwrap().cache_set(path, new_etag);
                }
                Some(etag) => {
                    if !new_etag.eq(&etag) {
                        debug!("Updating ETag from {:?} to {:?}", etag, new_etag);
                        self.etags.lock().unwrap().cache_set(path, new_etag);
                    } else {
                        debug!("Existing ETag {:?} matches. Not updating", etag);
                    }
                }
            }
        }
    }
}

#[async_trait::async_trait]
impl Middleware for TadoEtagMiddleware {
    async fn handle(
        &self,
        mut req: Request,
        extensions: &mut Extensions,
        next: Next<'_>,
    ) -> reqwest_middleware::Result<Response> {
        let url = req.url();
        let path = url.path().to_string();
        let etag = self.get_etag(path.to_string());
        debug!(
            "For path  {:?} got ETag  {:?} to use as IF_NONE_MATCH",
            path, etag
        );
        if etag.is_some() {
            let etag = etag.clone().unwrap();
            req.headers_mut()
                .insert(IF_NONE_MATCH, HeaderValue::from_str(etag.as_str()).unwrap());
        }
        let response = next.run(req, extensions).await;
        if response.is_ok() {
            self.update_etag(path.to_string(), etag.clone(), &response);
        }
        response
    }
}

lazy_static! {
    static ref HTTP_CLIENT_REQUESTS: HistogramVec = register_histogram_vec!(
        "http_client_requests",
        "help",
        &["method", "uri", "status", "outcome", "clientName"]
    )
    .unwrap();
}

struct HttpMetricsMiddleware {}

impl HttpMetricsMiddleware {
    pub fn new() -> Self {
        Self {}
    }

    fn client_name(req: &Request) -> String {
        let headers = req.headers();
        headers
            .get(USER_AGENT)
            .and_then(|h| h.to_str().ok())
            .map(|h| h.to_string())
            .unwrap_or_else(|| "Unknown".to_string())
    }

    fn method(req: &Request) -> String {
        req.method().to_string()
    }

    fn path(req: &Request) -> String {
        req.url().path().to_string()
    }
}

#[async_trait::async_trait]
impl Middleware for HttpMetricsMiddleware {
    async fn handle(
        &self,
        req: Request,
        extensions: &mut Extensions,
        next: Next<'_>,
    ) -> reqwest_middleware::Result<Response> {
        let start = SystemTime::now();
        let client_name = HttpMetricsMiddleware::client_name(&req);
        let method = HttpMetricsMiddleware::method(&req);
        let path = HttpMetricsMiddleware::path(&req);
        let response = next.run(req, extensions).await;

        let elapsed = SystemTime::now()
            .duration_since(start)
            .ok()
            .map(duration_to_seconds);
        let outcome = if response.is_ok() {
            "SUCCESS"
        } else {
            "FAILED"
        };
        let status = if let Ok(r) = response.as_ref() {
            r.status().to_string()
        } else {
            "0".to_string()
        };
        let metric = HTTP_CLIENT_REQUESTS.with_label_values(&[
            method.as_str(),
            path.as_str(),
            status.as_str(),
            outcome,
            client_name.as_str(),
        ]);
        if let Some(duration) = elapsed {
            metric.observe(duration);
        }
        response
    }
}

#[inline]
pub fn duration_to_seconds(d: Duration) -> f64 {
    let nanos = f64::from(d.subsec_nanos()) / 1e9;
    d.as_secs() as f64 + nanos
}

#[cfg(test)]
mod tests {
    use crate::TadoClient;
    use configuration::{Settings, TadoApiProps, TadoCredentials, TadoOauthProps};
    use reqwest::Url;
    use simple_logger::SimpleLogger;
    use std::sync::Once;
    use stubr::Stubr;

    static INIT: Once = Once::new();

    fn setup() {
        INIT.call_once(|| {
            SimpleLogger::new().init().unwrap();
        });
    }

    #[tokio::test]
    #[stubr::mock]
    async fn test_show_user() {
        setup();
        let settings = test_settings(&stubr);

        let tado_client = TadoClient::create(&settings).unwrap();

        let user = tado_client.show_user().await.unwrap().unwrap();
        assert_eq!(user.username, "test-user")
    }

    fn test_settings(stubr: &Stubr) -> Settings {
        let tado_oauth = TadoOauthProps {
            tokenurl: Url::parse(&stubr.path("/token")).unwrap(),
            clientid: "test-client-id".to_string(),
            clientsecret: "test-client-secret".to_string(),
            scopes: vec!["test-scope-1".to_string(), "test-scope-2".to_string()]
                .into_iter()
                .collect(),
        };
        let tado_credentials = TadoCredentials {
            username: "test-user".to_string(),
            password: "test-user-password-123".to_string(),
        };

        let tado_api_props = TadoApiProps {
            baseurl: Url::parse(&stubr.path("/api/v2")).unwrap(),
            oauth: Some(tado_oauth),
            credentials: Some(tado_credentials),
        };

        Settings {
            tadoapi: Some(tado_api_props),
            influx: None,
            metrics: None,
        }
    }
}
