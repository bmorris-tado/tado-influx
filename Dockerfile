FROM registry.access.redhat.com/ubi9/ubi-minimal

RUN mkdir -p "/opt/tado-influx"

WORKDIR "/opt/tado-influx"
COPY application.toml .
COPY target/release/tado-rust .

ENV RUN_MODE=production

ENTRYPOINT ["/opt/tado-influx/tado-rust"]